/**
 * Created by toinebakkeren on 23-03-16.
 */
var cacheHelper = {

    initialize: function() {
        pouchDB = new PouchDB("protodb");
    },

    /*
     "poster_path": "/kBf3g9crrADGMc2AMAMlLBgSm2h.jpg",
     "adult": false,
     "overview": "The Dark Knight of Gotham City begins his war on crime with his first major enemy being the clownishly homicidal Joker, who has seized control of Gotham's underworld.",
     "release_date": "1989-06-23",
     "genre_ids": [
     14,
     28
     ],
     "id": 268,
     "original_title": "Batman",
     "original_language": "en",
     "title": "Batman",
     "backdrop_path": "/2blmxp2pr4BhwQr74AdCfwgfMOb.jpg",
     "popularity": 4.378011,
     "vote_count": 929,
     "video": false,
     "vote_average": 6.78
     */
    getData: function(request, callback) {
        var self = this;

        console.log("Begin getData");

        pouchDB.get(request).catch(function (err) {
            if (err.status === 404) { // not found!
                self.putData(request, callback);
            } else { // hm, some other error
                throw err;
            }
        }).then(function (doc) {
            console.log(doc);
            ajax.parseJSONP(doc.data)
            self.putData(request,callback);
        })

        /*
        this.db.transaction(
            function(tx) {
                var sql = "SELECT data as response FROM movie WHERE request = ?;";
                tx.executeSql(sql, [request], function(tx, results) {
                    console.log(results);
                    if(results.rows.length !== 0) {
                        var response = results.rows.item(0).response;
                        console.log("De response is ready!");
                        callback(response);
                    }
                    else {
                        console.log("Launch putData");
                        self.putData(request, callback);
                    }
                });
            },
            this.txErrorHandler,
            function() {
                console.log('Table movie successfully CREATED in local SQLite database');
                callback();
            }
        );*/
    },

    putData: function(request, callback) {
        console.log("Begin putData");

        $.ajax({
            url: request,
            dataType: "jsonp",
            async: false,
            success: function (result) {
                var data = {"_id": request,"data": result}
                pouchDB.put(data);
                console.log('Synchronization complete items synchronized)');
                callback(result);

            },
            error: function (request,error) {
                alert('Network error has occurred please try again!');
            }
        });



        /*database.transaction(
            function(tx) {
                var sql = "INSERT OR REPLACE INTO movie (request, data) VALUES (?,?)";
                console.log('Inserting or Updating in local database:');


            },
            this.txErrorHandler
        );*/
    },

    txErrorHandler: function(tx) {
        //alert(tx.message);
        console.log(tx.message);
    }
}
